const video = document.getElementById('video');
const head = document.getElementById('Head');
const r_h = document.getElementById('right-hand');
const l_h = document.getElementById('left-hand');
const r_leg = document.getElementById('right-leg');
const l_leg = document.getElementById('left-leg');
const mouth = document.getElementById('mouth');



Promise.all([
  faceapi.nets.tinyFaceDetector.loadFromUri('/models'),
  faceapi.nets.faceLandmark68Net.loadFromUri('/models'),
  faceapi.nets.faceRecognitionNet.loadFromUri('/models'),
  faceapi.nets.faceExpressionNet.loadFromUri('/models')
]).then(startVideo)

function startVideo() {
  navigator.getUserMedia(
    { video: {} },
    stream => video.srcObject = stream,
    err => console.error(err)
  )
}


video.addEventListener('play', () => {
  const canvas = faceapi.createCanvasFromMedia(video)
  document.body.append(canvas)
  const displaySize = { width: video.width, height: video.height }
  faceapi.matchDimensions(canvas, displaySize)
  setInterval(async () => {
    const detections = await faceapi.detectAllFaces(video, new faceapi.TinyFaceDetectorOptions()).withFaceLandmarks().withFaceExpressions()
    const resizedDetections = faceapi.resizeResults(detections, displaySize)

    const expression = resizedDetections[0]?.expressions ?  "sad" : "happy";

    if (expression === "happy") {
      head.setAttribute("class", "active");
      r_h.setAttribute("class", "active");
      l_h.setAttribute("class", "active");
      r_leg.setAttribute("class", "active");
      l_leg.setAttribute("class", "active");
      mouth.setAttribute("class", "active");

    } else {
      head.removeAttribute("class");
      r_h.removeAttribute("class");
      l_h.removeAttribute("class");
      r_leg.removeAttribute("class");
      l_leg.removeAttribute("class");
      mouth.removeAttribute("class");
    }


    // canvas.getContext('2d').clearRect(0, 0, canvas.width, canvas.height)
    // faceapi.draw.drawDetections(canvas, resizedDetections)
    // faceapi.draw.drawFaceLandmarks(canvas, resizedDetections)
    // faceapi.draw.drawFaceExpressions(canvas, resizedDetections)
  }, 100)
})